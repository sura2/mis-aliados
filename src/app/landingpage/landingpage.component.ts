import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-landingpage',
  templateUrl: './landingpage.component.html',
  styleUrls: ['./landingpage.component.scss']
})
export class LandingpageComponent implements OnInit {
  activeLink = 1;
  showSearchOptions = false;
  searchSelected = 1;

  constructor() {
  }

  ngOnInit(): void {
  }
}
